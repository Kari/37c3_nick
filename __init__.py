from st3m.application import Application, ApplicationContext
from st3m.input import InputController, InputState
from st3m.ui.colours import PUSH_RED, GO_GREEN, BLACK
from st3m.goose import Dict, Any
from st3m.input import InputState
from ctx import Context
import leds

import json
import math


class Configuration:
    def __init__(self) -> None:
        self.name = "flow3r"
        self.size: int = 75
        self.font: int = 5

    @classmethod
    def load(cls, path: str) -> "Configuration":
        res = cls()
        try:
            with open(path) as f:
                jsondata = f.read()
            data = json.loads(jsondata)
        except OSError:
            data = {}
        if "name" in data and type(data["name"]) == str:
            res.name = data["name"]
        if "size" in data:
            if type(data["size"]) == float:
                res.size = int(data["size"])
            if type(data["size"]) == int:
                res.size = data["size"]
        if "font" in data and type(data["font"]) == int:
            res.font = data["font"]
        return res

    def save(self, path: str) -> None:
        d = {
            "name": self.name,
            "size": self.size,
            "font": self.font,
        }
        jsondata = json.dumps(d)
        with open(path, "w") as f:
            f.write(jsondata)
            f.close()

        
class NickApp37c3(Application):
    def __init__(self, app_ctx: ApplicationContext) -> None:
        super().__init__(app_ctx)
        self._scale = 1.0
        self._rot = 0.0
        self._led = 0.0
        self._idx_r = 0.0
        self._idx_g = 0.0
        self._idx_b = 0.0
        self._phase = 0.0
        self._time = 0
        self._led_rotation = 0
        self._led_mode = 0.0
        self._main_mode = -1
        self._anims = 4
        self._disp_mode = 0
        self._image_inc = 5
        self._image_max = 150
        self._image_idx = 0
        self._image_wait = 100
        self._wait_timer = 100
        self._path = app_ctx.bundle_path
        self._filename = "/flash/nick.json"
        self._run_in_sim = True
        self._config = Configuration.load(self._filename)


    def led_anim_1(self):
        for i in range(0,40):
            if i != int(self._led):
                leds.set_hsv(i, abs(self._scale) * 360, 1, 0.2)
            else:
                leds.set_rgb(i, 255, 255, 255)
        leds.set_rgb(int(self._idx_r), 255,0,0)
        leds.set_rgb(39-int(self._idx_g), 0,255,0)
        leds.set_rgb(39-int(self._idx_b), 0,0,255)


    def led_anim_2(self):
        for i in range(0,40):
            leds.set_hsv(i, 0,0,0)
        leds.set_rgb(int(self._led), 255, 255, 255)
        leds.set_rgb(39-int(self._led), 255, 255, 255)

    def led_anim_3(self):
        for i in range(0,40):
            leds.set_hsv(i, 0,0,0)
        x =   int(self._led)%4
        leds.set_rgb( 0+ 4-x, 0,255, 0)
        leds.set_rgb( 0+ 4+x, 0,255, 0)
        leds.set_rgb( 8+ 4-x, 0,255, 0)
        leds.set_rgb( 8+ 4+x, 0,255, 0)
        leds.set_rgb(16+ 4-x, 0,255, 0)
        leds.set_rgb(16+ 4+x, 0,255, 0)
        leds.set_rgb(24+ 4-x, 0,255, 0)
        leds.set_rgb(24+ 4+x, 0,255, 0)
        leds.set_rgb(32+ 4-x, 0,255, 0)
        leds.set_rgb(32+ 4+x, 0,255, 0)

    def led_anim_4(self):
        # Rahix' Nick
        color1 = 274
        color2 = 329
        for i in range(21):
            i20 = i / 20
            hue = i20 * color2 + (1 - i20) * color1
            value = i20 * 0.5 + (1 - i20) * 0.2
            leds.set_hsv((i + self._led_rotation) % 20, hue, 1, value)
            leds.set_hsv(39 - (i + self._led_rotation) % 20, hue, 1, value)


    def draw(self, ctx: Context) -> None:
        ctx.text_align = ctx.CENTER
        ctx.text_baseline = ctx.MIDDLE
        ctx.font = ctx.get_font_name(1)

        ctx.rgb(0, 0, 0).rectangle(-120, -120, 240, 240).fill()

        if self._disp_mode > 0:
            ctx.save()
            ctx.font_size = 15
            ctx.move_to(0, -100)
            
            a = 1 if self._disp_mode > 1000 else self._disp_mode / 1000.0
            ctx.rgba(1,1,1,a)
            if self._main_mode < 0:
                ctx.text(f"Mode ALL")    
            else:
                ctx.text(f"Mode {self._main_mode+1}")
            ctx.restore()

        ctx.image(f"{self._path}/images/37C3.png", -70, -90, -1, -1)
        path = f"{self._path}/images/37C3_{self._image_idx:03}.png"
        ctx.image(path, -90, -25, -1, -1)
        
        ctx.font_size = self._config.size
        
        ctx.move_to(0, 0)
        ctx.save()
        ctx.translate(0,60)
        
        angle = 0.4 * self._rot
        ctx.rotate(angle)

        ctx.scale(0.9, 0.9)
        size = 0.6 + 0.3*math.cos(self._scale*0.7+1)
        ctx.scale(size, size)
        
        ctx.rgb(1,1,1)
        ctx.text(self._config.name)
        ctx.restore()
      
        mode = self._led_mode if self._main_mode == -1 else self._main_mode

        if mode < 1:
            self.led_anim_1()
        elif mode < 2:
            self.led_anim_2()
        elif mode < 3:
            self.led_anim_3()
        else:
            self.led_anim_4()

        leds.update()

    def on_exit(self) -> None:
        # self._config.save(self._filename)
        pass

    def think(self, ins: InputState, delta_ms: int) -> None:
        super().think(ins, delta_ms)
         
        if self.input.buttons.app.left.pressed:
            self._main_mode = self._anims-1 if self._main_mode == -1 else self._main_mode -1
            self._disp_mode = 3000

        elif self.input.buttons.app.right.pressed:
            self._main_mode = -1 if self._main_mode == self._anims-1 else self._main_mode +1
            self._disp_mode = 3000

        if self._disp_mode > 0:
            self._disp_mode -= delta_ms


        self._time += delta_ms / 1000
        self._led_rotation = math.floor(self._time * 5) % 40

        sec = delta_ms / 1000

        self._phase += sec
        self._scale = math.sin(self._phase * 1.1)
        self._rot = math.sin(self._phase * 1.5 )


        self._led_mode += sec / 6
        if self._led_mode >= self._anims:
            self._led_mode = 0.0

        if self._led_mode < 1:
            self._led += delta_ms / 90
        elif self._led_mode < 2:
            self._led += delta_ms / 40
        else:      
            self._led += delta_ms / 250

        if self._led >= 40:
            self._led = 0


        self._idx_r += delta_ms / 60
        if self._idx_r >= 40:
            self._idx_r = 0

        self._idx_g += delta_ms / 67
        if self._idx_g >= 40:
            self._idx_g = 0

        self._idx_b += delta_ms / 56
        if self._idx_b >= 40:
            self._idx_b = 0


        if self._wait_timer > 0:
            self._wait_timer -= delta_ms
        else:
            self._wait_timer = self._image_wait
            self._image_idx += self._image_inc
            if self._image_idx > self._image_max:
                self._image_idx = 0
                
